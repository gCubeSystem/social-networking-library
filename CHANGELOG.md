
# Changelog for Social Networking Library

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.1.0] - 2024-10-28

 - maven-portal-bom 4.0.0-SNAPSHOT
 - [StorageHub] downstream components to upgrade in order to work with storagehub 1.5.0 [#27999]

## [v2.0.2] - 2024-10-14

 - Mail notification Catalogue Item Rejected [#28020]

## [v2.0.1] - 2024-04-22

 - Null pointer exception getting notifications preferences fixed [#27218]
 - Removed noisy logs [#27286]

## [v2.0.0] - 2023-12-04

 - Support for Cassandra 4.1.3 using DataStax java driver

## [v1.17.0] - 2022-05-13

 - Added support for Catalogue notifications
 - Ported to git

## [v1.16.1] - 2018-03-07

 - Added JsInterop DTOs (ClientPost, ClientAttachment and JSON) for supporting new IPC Client side in NewsFeed
 
## [v1.16.0] - 2017-05-25

 - Added feature for hashtags in comments
 
## [v1.15.0] - 2017-01-25

 - Added support for job completion notifications
 - Improved exceptions handling
 
## [v1.14.0] - 2016-09-29

 - Upgraded astyanax dependency to 2.0.2
 - Removed support for Document Workflow notification
  
## [v1.13.0] - 2016-09-05

 - Method to close connections pool to Apache Cassandra added
 - Added methods to retrieve recent user's commented and liked posts
 - Added enum class ShowUserStatisticAction

## [v1.10.0] - 2016-01-22

 - Multi attachments to posts [#1982]
 
## [v1.9.0] - 2016-01-15

 - For user statistics fast retrieval [#1663]
 - Updated the way we instanciate keyspace, now it is more efficient [#1493]
 - Updated methods for editing comments [#246]

## [v1.8.0] - 2015-07-03

 - Added feature for invites and hashtags
 
## [v1.6.0] - 2014-04-08

 - Added feature for post retrieval by range
 - Added feature for notifications retrieval by range
 - Added feature for unlike
  
## [v1.5.0] - 2014-03-05

 - Added feature for post alert notifications

## [v1.3.0] - 2013-07-08

 - Added feature for calendar notifications
 - Added feature for URL encoding decoding js base 6
 
## [v1.2.0] - 2013-05-29

 - Added feature for fine grained notifications
 
## [v1.1.0] - 2013-04-19

 - Added feature for people taggings (mentions)
 - Fix for method getAllPortalPrivacyLevelFeeds() not recongnizing application posts

## [v1.0.0] - 2013-01-11

- First release
