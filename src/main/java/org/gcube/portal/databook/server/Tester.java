package org.gcube.portal.databook.server;

import org.gcube.portal.databook.shared.*;
import org.gcube.portal.databook.shared.ex.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author Massimiliano Assante ISTI-CNR
 * @author Ahmed Ibrahim ISTI-CNR
 *
 * @version 2.0.0 October 2023
 *
 */
public class Tester {
    private static DBCassandraAstyanaxImpl store;
    private static Logger LOGGER = LoggerFactory.getLogger(Tester.class);

    public Tester() {
        store = new DBCassandraAstyanaxImpl("gcube"); //set to true if you want to drop the KeySpace and recreate it
        
    }

    public static void main(String[] args) throws ColumnNameNotFoundException, PrivacyLevelTypeNotFoundException, FeedIDNotFoundException, FeedTypeNotFoundException {
        Tester test = new Tester();
        //test.getComment();
        test.testFunc();
        System.exit(0);

    }
    public  void testFunc() throws ColumnNameNotFoundException, PrivacyLevelTypeNotFoundException, FeedIDNotFoundException, FeedTypeNotFoundException {
        String postIdToUpdate = "047c601d-2291-4974-9224-d6732b1fbe26";
        Post read = store.readPost(postIdToUpdate);

        List<Comment> readC = store.getAllCommentByPost("047c601d-2291-4974-9224-d6732b1fbe26");
        System.out.println(read);
        readC.forEach(c -> System.out.println(c.getText()));
    }
    public void getComment(){
        String uuid = "820969b2-4632-4197-9fd6-5aafab781faa";

        Comment c;
        try {
            c = store.readCommentById(uuid);
            System.out.println(c);
        } catch (CommentIDNotFoundException e) {
            // TODO Auto-generated catch block
            System.err.println(e.toString());
        }
    }
}