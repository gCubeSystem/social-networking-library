package org.gcube.portal.databook.server;

/**
 * @author Massimiliano Assante ISTI-CNR
 * @author Ahmed Ibrahim ISTI-CNR
 *
 * @version 2.0.0 October 2023
 *
 */

public class Schema {
    //Tables
    public static final String NOTIFICATIONS = "Notifications";
    public static final String POSTS = "Posts";
    public static final String COMMENTS = "Comments";
    public static final String LIKES = "Likes";
    public static final String INVITES = "Invites";
    public static final String VRE_TIMELINE_POSTS = "VRETimeline";
    public static final String USER_TIMELINE_POSTS = "UserTimeline";
    public static final String APP_TIMELINE_POSTS = "AppTimeline";
    public static final String USER_LIKED_POSTS = "UserLikes";
    public static final String USER_NOTIFICATIONS = "UserNotifications"; // regular user notifications timeline (both read and unread, messages are included)
    public static final String USER_NOTIFICATIONS_UNREAD = "UserUnreadNotifications"; // only unread user notifications/ notifications messages
    public static final String USER_NOTIFICATIONS_PREFERENCES = "UserNotificationsPreferences"; // preferences for notifications
    public static final String HASHTAGS_COUNTER = "HashtagsCounter"; // count the hashtags per group and type
    public static final String HASHTAGGED_POSTS = "HashtaggedPosts"; // contains hashtags per type associated with vre and POST
    public static final String HASHTAGGED_COMMENTS = "HashtaggedComments"; // contains hashtags per type associated with vre and comment
    public static final String VRE_INVITES = "VREInvites"; //contains the emails that were invited per VRE
    public static final String EMAIL_INVITES = "EmailInvites"; //contains the list of invitation per email
    public static final String ATTACHMENTS = "Attachments"; //contains the list of all the attachments in a POST

    //columns
    public static final String USER_ID = "userid"; //text
    public static final String TYPE = "type";   //text
    public static final String PREFERENCE = "preference";  //text
    public static final String TIMESTAMP = "timestamp";   //timestamp
    public static final String NOT_ID = "notid";   //UUID
    public static final String VRE_ID = "vreid";   //text
    public static final String POST_ID = "postid";  //UUID
    public static final String APP_ID = "appid";   //text
    public static final String HASHTAG = "hashtag"; //text
    public static final String COMMENT_ID = "commentid"; //UUID
    public static final String COUNT = "count";  //big int
    public static final String LIKE_ID = "likeid"; //UUID
    public static final String INVITE_ID = "inviteid";  //UUID
    public static final String STATUS = "status"; //text
    public static final String EMAIL = "email";  //text
    public static final String ATTACH_ID = "attachid"; //UUID
    public static final String URI = "uri"; //text
    public static final String NAME = "name"; //text
    public static final String DESCRIPTION = "description"; //text
    public static final String URI_THUMBNAIL = "urithumbnail"; //text
    public static final String MIME_TYPE = "mimetype";  //text
    public static final String SENDER_USER_ID = "senderuserid"; //text
    public static final String CONTROL_CODE = "controlcode"; //text
    public static final String SENDER_FULL_NAME = "senderfullname"; //text
    public static final String FULL_NAME = "fullname"; //text
    public static final String THUMBNAIL_URL = "thumbnailurl"; //text
    public static final String COMMENT = "comment"; //text
    public static final String IS_EDIT = "isedit"; //bool
    public static final String LAST_EDIT_TIME = "lastedittime"; //timestamp
    public static final String SUBJECT_ID = "subjectid"; //text
    public static final String SENDER_ID = "senderid"; //text
    public static final String SENDER_THUMBNAIL_URL = "senderthumbnailurl"; //text
    public static final String IS_READ = "isread"; //bool
    public static final String LINK_HOST = "linkhost"; //text
    public static final String LIKES_NO = "likesno"; //big int
    public static final String LINK_DESCRIPTION = "linkdescription"; //text
    public static final String IS_APPLICATION_POST = "isapplicationpost"; //bool -->
    public static final String ENTITY_ID = "entityid"; //text
    public static final String PRIVACY = "privacy"; //text
    public static final String MULTI_FILE_UPLOAD = "multifileupload"; //bool
    public static final String COMMENTS_NO = "commentsno"; //big int
    public static final String LINK_TITLE = "linktitle"; //text

}