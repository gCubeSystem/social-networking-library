package org.gcube.portal.databook.shared.ex;

@SuppressWarnings("serial")
public class NotificationTypeNotFoundException  extends Exception {
	public NotificationTypeNotFoundException(String message) {
		super(message);
	}
}
