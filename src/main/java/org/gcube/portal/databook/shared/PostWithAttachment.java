package org.gcube.portal.databook.shared;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class PostWithAttachment implements Serializable {
    private Post post;
    private List<Attachment> attachments;

    public PostWithAttachment(Post post, List<Attachment> attachments){
        super();
        this.post = post;
        this.attachments = attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }
    public void setPost(Post post) {
        this.post = post;
    }


    public List<Attachment> getAttachments() {
        return attachments;
    }

    public Post getPost() {
        return post;
    }

    public String toString() {
        String postInfo = "Post [key=" + post.getKey() + ", type=" + post.getType() + ", entityId=" + post.getEntityId()
                + ", time=" + post.getTime() + ", vreid=" + post.getVreid() + ", uri=" + post.getUri()
                + ", uriThumbnail=" + post.getUriThumbnail() + ", description="
                + post.getDescription() + ", privacy=" + post.getPrivacy() + ", fullName="
                + post.getFullName() + ", email=" + post.getEmail() + ", thumbnailURL="
                + post.getThumbnailURL() + ", commentsNo=" + post.getCommentsNo() + ", likesNo="
                + post.getLikesNo() + ", linkTitle=" + post.getLinkTitle() + ", linkDescription="
                + post.getLinkDescription() + ", linkHost=" + post.getLinkHost()
                + ", applicationFeed=" + post.isApplicationFeed()
                + ", multiFileUpload=" + post.isMultiFileUpload() + "]";
        String attachmentInfo = "[Attachments: ";
        for (Attachment attachment: attachments){
            attachmentInfo += "[Attachment [key=" + attachment.getId() + ", uri=" + attachment.getUri() + ", name=" + attachment.getName() + ", description="
                    + attachment.getDescription() + ", thumbnailURL=" + attachment.getThumbnailURL()
                    + ", mimeType=" + attachment.getMimeType() + "]],";
        }
        attachmentInfo = attachmentInfo.substring(0, attachmentInfo.length()-1);
        attachmentInfo += "]";
        ;

        return "[" + postInfo + ", " + attachmentInfo + "]" ;
    }
}
